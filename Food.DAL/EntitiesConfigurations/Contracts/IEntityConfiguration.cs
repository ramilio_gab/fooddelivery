﻿using System;
using FoodDelivery.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FoodDelivery.DAL.EntitiesConfigurations
{
    public interface IEntityConfiguration<T> where T : class, IEntity

    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
