﻿namespace FoodDelivery.DAL.Entities
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
