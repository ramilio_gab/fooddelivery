﻿using Microsoft.AspNetCore.Identity;

namespace FoodDelivery.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
    }
}
